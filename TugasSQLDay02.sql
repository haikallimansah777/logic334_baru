---TugasSQLDay02---

---Membuat Database---
CREATE DATABASE DB_Entertainer

CREATE TABLE artis(
	id int PRIMARY KEY identity (1,1) NOT NULL,
	kd_artis varchar(100) NOT NULL,
	nm_artis varchar(100) NOT NULL,
	jk varchar(100) NOT NULL,
	bayaran bigint NOT NULL,
	award int NOT NULL,
	negara Varchar(100) NOT NULL
)

CREATE TABLE film(
	id int PRIMARY KEY identity (1,1) NOT NULL,
	kd_film varchar(10) NOT NULL,
	nm_film varchar(55) NOT NULL,
	genre varchar(55) NOT NULL,
	artis varchar(55) NOT NULL,
	produser varchar(55) NOT NULL,
	pendapatan bigint NOT NULL,
	nominasi int
)

CREATE TABLE produser(
	id int PRIMARY KEY identity (1,1) NOT NULL,
	kd_produser varchar(50) NOT NULL,
	nm_produser varchar(50) NOT NULL,
	international varchar (50) NOT NULL
)

CREATE TABLE negara(
	id int PRIMARY KEY identity (1,1) NOT NULL,
	kd_negara varchar(100) NOT NULL,
	nm_negara varchar(100) NOT NULL
)

CREATE TABLE genre(
	id int PRIMARY KEY identity (1,1) NOT NULL,
	kd_genre varchar(50) NOT NULL,
	nm_genre varchar(50) NOT NULL
)

--insert data
INSERT INTO artis(kd_artis,nm_artis,jk,bayaran,award,negara)
VALUES
('A001','ROBERT DOWNEY JR','PRIA','3000000000','2','AS'),
('A002','ANGELINA JOLIE','WANITA','700000000','1','AS'),
('A003','JACKIE CHAN','PRIA','200000000','7','HK'),
('A004','JOE TASLIM','PRIA','350000000','1','ID'),
('A005','CHELSEA ISLAN','WANITA','300000000','0','ID')

INSERT INTO film(kd_film,nm_film,genre,artis,produser,pendapatan,nominasi)
VALUES
('F001','IRON MAN','G001','A001','PD01','2000000000','3'),
('F002','IRON MAN 2','G001','A001','PD01','1800000000','2'),
('F003','IRON MAN 3','G001','A001','PD01','1200000000','0'),
('F004','AVENGER: CIVIL WAR','G001','A001','PD01','2000000000','1'),
('F005','SPIDER MAN HOME COMING','G001','A001','PD01','1300000000','0'),
('F006','THE RAID','G001','A004','PD03','800000000','5'),
('F007','FAST & FURIOUS','G001','A004','PD05','830000000','2'),
('F008','HABIBIE DAN AINUN','G004','A005','PD03','670000000','4'),
('F009','POLICE STORY','G001','A003','PD02','700000000','3'),
('F010','POLICE STORY 2','G001','A003','PD02','710000000','1'),
('F011','POLICE STORY 3','G001','A003','PD02','615000000','0'),
('F012','RUSH HOUR','G003','A003','PD05','695000000','2'),
('F013','KUNGFU PANDA','G003','A003','PD05','923000000','5')

INSERT INTO produser(kd_produser, nm_produser, international)
VALUES
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAPI FILM','TIDAK'),
('PD04','PARKIT','TIDAK'),
('PD05','PARAMOUNT PICTURE','YA')

INSERT INTO negara(kd_negara, nm_negara)
VALUES
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')

INSERT INTO genre(kd_genre, nm_genre)
VALUES
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILLER'),
('G006','FICTION')

---1. Menampilkan jumlah pendapatan produser marvel secara keseluruhan
SELECT p.nm_produser, SUM(f.pendapatan) as pendapatan
FROM film AS f
JOIN produser AS p on p.kd_produser = f.produser
GROUP BY p.nm_produser
HAVING p.nm_produser = 'MARVEL'

--2. Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi
SELECT nm_film, nominasi
FROM film
WHERE nominasi = 0

--3. Menampilkan nama film yang huruf depannya 'p'
SELECT nm_film
FROM film
--WHERE nm_film LIKE 'p%'
WHERE SUBSTRING(nm_film, 1, 1) = 'p'

--4. Menampilkan nama film yang huruf terakhir 'y'
SELECT nm_film
FROM film
--WHERE nm_film LIKE '%y'
WHERE SUBSTRING(nm_film, DATALENGTH(nm_film),1) = 'y'

--5. Menampilkan nama film yang mengandung huruf 'd'
SELECT nm_film
FROM film
WHERE nm_film LIKE '%d%'

--6. Menampilkan nama film dan artis
SELECT f.nm_film, a.nm_artis
FROM film AS f
JOIN artis AS a on f.artis = a.kd_artis
--WHERE f.nm_film like '%IRON%' or a.nm_artis like '%JACKIE%'

--7. Menampilkan nama film yang artisnya berasal dari negara hongkong
SELECT f.nm_film, a.negara
FROM film AS f
right JOIN artis AS a on f.artis = a.kd_artis
WHERE negara = 'HK'

--8. Menampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf 'o'
SELECT f.nm_film, n.nm_negara
FROM film AS f
JOIN artis AS a 
on f.artis = a.kd_artis
JOIN negara as n
on n.kd_negara = a.negara
WHERE n.nm_negara not LIKE '%o%'

--9. Menampilkan nama artis yang tidak pernah bermain film
SELECT a.nm_artis
FROM artis AS a
LEFT JOIN film AS f on f.artis = a.kd_artis
WHERE f.artis is null

--10. Menampilkan nama artis yang bermain film dengan genre drama
SELECT a.nm_artis, g.nm_genre
FROM artis as a
JOIN film as f on f.artis = a.kd_artis
JOIN genre as g on f.genre = g.kd_genre
WHERE g.nm_genre = 'DRAMA'

--11. Menampilkan nama artis yang bermain film dengan genre Action
SELECT a.nm_artis, g.nm_genre
FROM artis as a
JOIN film as f on f.artis = a.kd_artis
JOIN genre as g on f.genre = g.kd_genre
WHERE g.nm_genre = 'ACTION'
GROUP BY a.nm_artis, g.nm_genre

--12. Menampilkan data negara dengan jumlah filmnya
SELECT n.kd_negara, n.nm_negara, count(negara) as jumlah_Film
FROM film as f
JOIN artis as a on f.artis = a.kd_artis
RIGHT JOIN negara as n on a.negara = n.kd_negara
GROUP BY n.kd_negara, n.nm_negara


--13. Menampilkan nama film yang skala internasional
SELECT f.nm_film
FROM film as f
JOIN produser as p on f.produser = p.kd_produser
WHERE p.international = 'YA'


--14. Menampilkan jumlah film dari masing2 produser
SELECT p.nm_produser, COUNT(kd_film) as Jumlah_Film
FROM film as f
RIGHT JOIN produser as p on p.kd_produser = f.produser
GROUP BY p.nm_produser
--HAVING count(kd_film) >=3 and nm_produser = 'MARVEL'
