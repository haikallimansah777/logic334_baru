--Untuk Convert :
--CONVERT (NUMERIC(18,1),(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)) as total_gaji
--Untuk membulatkan : 
select ROUND(6125.3412,2)
--Membulatkan kebawah : 
select FLOOR(6125.6412)
--Membulatkan keatas :
select CEILING(6125.6412)

---Create STORED PROCEDURE (SP)
CREATE procedure sp_RetieveMahasiswa
	--parameter di sini
  as
  begin
	SELECT name, address, email, nilai
	FROM mahasiswa
  end

--RUN/EXEC PROSEDURE
  exec sp_RetieveMahasiswa

--EDIT/ALTER SP
ALTER procedure sp_RetieveMahasiswa
	--parameter di sini
	@id int,
	@nama varchar(50)
  as
  begin
	SELECT name, address, email, nilai
	FROM mahasiswa
	WHERE id = @id and name = @nama
  end

--RUN/EXEC PROSEDURE WITH PARAMETER
  exec sp_RetieveMahasiswa 1,'haikal limansah'

 --FUNCTION
 --CREATE FUNCTION
 CREATE FUNCTION fn_total_mahasiswa
 (
	@id int
 )
 RETURNS int
 BEGIN
	DECLARE @hasil int
	SELECT @hasil = count(id) FROM mahasiswa
	WHERE id = @id
	RETURN @hasil
 END

 --EDIT ATAU ALTER FUNCTION
ALTER FUNCTION fn_total_mahasiswa
 (
	@id int,
	@nama VARCHAR(50)
 )
 RETURNS int
 BEGIN
	DECLARE @hasil int
	SELECT @hasil = count(id) FROM mahasiswa
	WHERE id = @id and name = @nama
	RETURN @hasil
 END

--RUN FUNCTION
  SELECT dbo.fn_total_mahasiswa(1,'Haikal Limansah')

--SELECT FUNCTION MAHASISWA
SELECT mhs.id, mhs.name, dbo.fn_total_mahasiswa(mhs.id,mhs.name) as fungsi
FROM mahasiswa as mhs
JOIN biodata as bio on bio.mahasiswa_id = mhs.id

--TRUNCATE TABLE (UNTUK DELETE DAN RESET ISI TABEL)
TRUNCATE TABLE [namatable]