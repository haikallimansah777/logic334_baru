﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'plusMinus' function below.
     *
     * The function accepts INTEGER_ARRAY arr as parameter.
     */

    public static void plusMinus(List<int> arr)
    {
        int n = arr.Count;
        double a = 0;
        double b = 0;
        double c = 0;

        for (int i = 0; i < n; i++)
        {
            if (arr[i] > 0)
            {
                a = a + 1;
            }
            else if (arr[i] < 0)
            {
                b = b + 1;
            }
            else
                c = c + 1;
        }
        a = a / n;
        b = b / n;
        c = c / n;

        a.ToString("N6");
        b.ToString("N6");
        c.ToString("N6");

        Console.WriteLine($"{a}");
        Console.WriteLine($"{b}");
        Console.WriteLine($"{c}");

        /*        Console.WriteLine((double)a / n);
                Console.WriteLine((double)b / n);
                Console.WriteLine((double)c / n);*/

    }

}

class Solution
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        Result.plusMinus(arr);
    }
}
