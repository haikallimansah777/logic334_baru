﻿//Nomor1();
//Nomor2();
//Nomor3();
//Nomor4();
//Nomor5();
//Nomor6();
//Nomor7();
Nomor8();

//Nomor9();
//Nomor10();
Console.ReadKey();

static void Nomor1()
{
    Console.WriteLine("===Jumlah cara duduk di bangku berdasarkan jumlah anak===");
    Console.Write("Masukan jumlah anak : ");
    int n = int.Parse(Console.ReadLine());
    int val = 1;

    string detail = "";

    if (n > 0)
    {
        for (int i = n; i >= 1; i--)
        {
            val = val * i;
            Console.Write($"{i} x ");
            detail += detail == "" ? i : " x " + i;
        }
    }
    else
        val = 0;

    Console.Write($"Jumlah cara : {val}");
}

static void Nomor2()
{
    Console.WriteLine("---Menghitung sinyal SOS yang salah---");
    Console.Write("Masukan input : ");
    string input = Console.ReadLine().ToUpper();
    //char[] input = Console.ReadLine().ToUpper().ToCharArray();
    //char[] baru = Array.ConvertAll(input,char.Parse);
    int jumlahB = 0;
    int jumlahS = 0;
    string hasil = "";
    string sos = "sosSOS";
    string sosvalid = "";


    Console.WriteLine($"Sinyal yang diterima signal : {input}");

    /*    foreach (char c in input)
        {
            if (char.IsLetter(c))
            {
                if ("SOS".Contains(c))
                {
                    jumlahB++;
                }
                else
                {
                    jumlahS++;
                }
            }
        }*/

    if ((input.Length % 3) != 0)
    {
        Console.WriteLine("Input invalid");
    }
    else 
    {
        for (int i = 0; i < input.Length; i += 3)
        {
            if ("SOS" == input.Substring(i, 3))
            {
                jumlahB++;
            }
            else
            {
                jumlahS++;
            }
            sosvalid += "SOS";
        }
        Console.WriteLine($"Keterangan sinyal yang benar : {sosvalid}");
        Console.WriteLine($"Total sinyal benar : {jumlahB}");
        Console.WriteLine($"Total sinyal salah : {jumlahS}");
    }

    /*    foreach(char c in input)
        {
            for(int i = 0; i < input.Length; i++)
            {
                if (input[i] != 'S' || input[i + 1] !+ 'O')
            }
        }*/



    /*    foreach(char ch in input)
        {
            if(char)
        }*/

    /*    foreach (char ch in input)
        {
            if (char.IsLetter(ch) && sos.Contains(ch))
            {
                hasil += ch;
            }
        }*/

    //  Console.WriteLine($"Sinyal yang benar : {hasil}");

}

static void Nomor3()
{
    Console.WriteLine("---Program Perhitungan Denda Peminjaman Buku---");
    try
    {
        Console.Write("Masukkan tanggal peminjaman (dd-MM-yyyy): ");
        string inputTanggalPeminjaman = Console.ReadLine();
        DateTime tanggalPeminjaman = DateTime.ParseExact(inputTanggalPeminjaman, "dd-MM-yyyy", null);

        Console.Write("Masukkan tanggal pengembalian (dd-MM-yyyy): ");
        string inputTanggalPengembalian = Console.ReadLine();
        DateTime tanggalPengembalian = DateTime.ParseExact(inputTanggalPengembalian, "dd-MM-yyyy", null);

        if (tanggalPengembalian < tanggalPeminjaman)
        {
            Console.WriteLine("Tanggal pengembalian tidak boleh sebelum tanggal peminjaman.");
        }
        else
        {
            TimeSpan selisih = tanggalPengembalian - tanggalPeminjaman;
            Console.Write($"{selisih}");
            int selisihHari = (int)selisih.TotalDays;
            int totalDenda;
            if(selisihHari <= 3)
            {
                totalDenda = 0;
            }
            else
            {
                int besarDendaPerHari = 500;
                totalDenda = selisihHari * besarDendaPerHari;
            }
            Console.WriteLine("Selisih hari: " + selisihHari + " hari");
            Console.WriteLine("Denda yang harus dibayar: " + totalDenda + " Rupiah");
        }
    }
    catch (FormatException)
    {
        Console.WriteLine("Format tanggal salah. Pastikan menggunakan format dd-MM-yyyy.");
    }
}

static void Nomor4()
{
    Console.WriteLine("---Penentuan Tanggal Ujian----");
    Console.Write("Masukkan tanggal masuk kelas (dd/MM/yyy): ");
    DateTime tanggalMasuk = DateTime.ParseExact(Console.ReadLine(), "dd/MM/yyyy", null);
    Console.WriteLine("Masukkan tanggal libur (pisahkan dengan ,) :");
    string tanggalLiburstr = Console.ReadLine();
    string[] tanggalLibur = tanggalLiburstr.Split(',');
    int totalLibur = tanggalLibur.Length;

    for (int i = 0; i < totalLibur; i++)
    {
        tanggalMasuk = tanggalMasuk.AddDays(1);
        while (tanggalMasuk.DayOfWeek == DayOfWeek.Saturday || tanggalMasuk.DayOfWeek == DayOfWeek.Sunday)
        {
            tanggalMasuk = tanggalMasuk.AddDays(1);
        }
    }

    for (int i = 0; i < 10; i++)
    {
        tanggalMasuk = tanggalMasuk.AddDays(1);
        while (tanggalMasuk.DayOfWeek == DayOfWeek.Saturday || tanggalMasuk.DayOfWeek == DayOfWeek.Sunday)
        {
            tanggalMasuk = tanggalMasuk.AddDays(1);
        }
    }

    Console.WriteLine("Tanggal ujian : " + tanggalMasuk.ToString("dd/MM/yyyy"));



    /*    int hariKerja = 0;
        for (int i = 1; i <= 11; i++)
        {
            DateTime tanggal = tanggalMasuk.AddDays(i);

            if (tanggal.DayOfWeek != DayOfWeek.Saturday && tanggal.DayOfWeek != DayOfWeek.Sunday && !tanggalLibur.Contains(tanggal))
            {
                hariKerja++;
            }
        }

        DateTime tanggalUjian = tanggalMasuk;

        while (hariKerja > 0)
        {
            tanggalUjian = tanggalUjian.AddDays(1);

            if (tanggalUjian.DayOfWeek != DayOfWeek.Saturday && tanggalUjian.DayOfWeek != DayOfWeek.Sunday && !tanggalLibur.Contains(tanggalUjian))
            {
                hariKerja--;
            }
        }

        Console.WriteLine("Tanggal ujian: " + tanggalUjian.ToString("dd/MM/" +"yyyy"));*/
}

static void Nomor5()
{
    Console.WriteLine("---Menghitung huruf vokal dan konsonan---");
    Console.Write("Masukan input : ");
    string input = Console.ReadLine().ToLower();

    int jumlahV = 0;
    int jumlahK = 0;

    foreach (char c in input)
    {
        if (char.IsLetter(c))
        {
            if ("aiueo".Contains(c))
            {
                jumlahV++;
            }
            else
            {
                jumlahK++;
            }
        }
    }

    Console.WriteLine($"Jumlah huruf vokal : {jumlahV}");
    Console.WriteLine($"Jumlah huruf konsonan : {jumlahK}");

}

static void Nomor6()
{
    Console.WriteLine("---Nama---");
    Console.Write("Input nama : ");
    string input = Console.ReadLine().ToLower();
    
    foreach(char c in input)
    {
        Console.WriteLine($"***{c}***");
    }
}

static void Nomor7()
{
    Console.WriteLine("---Hitung bill elsa---");
    Console.Write("Jumlah total menu yang dipesan: ");
    int jumlahMenu = int.Parse(Console.ReadLine());
    Console.Write("Index menu yang tidak dimakan oleh Elsa: ");
    int indexMenuAlergi = int.Parse(Console.ReadLine());
    Console.WriteLine("Harga menu per item (masukkan harga masing-masing menu): ");
    int[] hargaMenu = new int[jumlahMenu];
    for (int i = 0; i < jumlahMenu; i++)
    {
        Console.Write($"Harga menu ke-{i}: ");
        hargaMenu[i] = int.Parse(Console.ReadLine());
    }

    Console.Write("Uang Elsa: ");
    int uangElsa = int.Parse(Console.ReadLine());

    int totalHarga = 0;

    for (int i = 0; i < jumlahMenu; i++)
    {
        if (i == indexMenuAlergi)
        {
            continue;
        }
        totalHarga += hargaMenu[i];
    }
    totalHarga = totalHarga / 2;
    int sisaUangElsa = uangElsa - totalHarga;

    if (sisaUangElsa >= 0)
    {
        Console.WriteLine("Elsa harus membayar: " + totalHarga);
        Console.WriteLine("Sisa uang Elsa: " + sisaUangElsa);
    }
    else
    {
        Console.WriteLine("Uang Elsa kurang: " + (-sisaUangElsa));
    }
}


static void Nomor8()
{
    Console.WriteLine("---Staircase---");
    Console.Write("Masukan input : ");
    int n = int.Parse(Console.ReadLine());

    for (
        int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            if (j <= n - i)
            {
                Console.Write(" ");
            }
            else
            {
                Console.Write("#");
            }
        }
        Console.WriteLine();
    }
    for (int i = 1; i <= n; i++)
    {
        Console.WriteLine(new string(' ', n - i) + new string('#', i));
    }
}

static void Nomor9()
{
    Console.WriteLine("---Array 2 Dimensi 3x3---");
    int[,] array = new int[3, 3];
    Console.WriteLine("Masukkan elemen-elemen array:");

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            Console.Write($"Value dari index [{i},{j}]: ");
            array[i, j] = int.Parse(Console.ReadLine());
        }
    }

    Console.WriteLine("Array 3x3 yang diinput :");
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            Console.Write(array[i, j] + " ");
        }
        Console.WriteLine();
    }

    Console.WriteLine("===Perhitungan diagonal===");
    Console.WriteLine("Diagonal 1 : ");
    int val1 = 0;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (i == j)
            {
                Console.Write(array[i, j] + " ");
            }
            else
            {
                Console.Write("  ");
            }
        }
        Console.WriteLine();
    }
    for (int i = 0; i < 3; i++)
    {
        int angka1 = array[i, i];
        Console.Write(angka1 + " + ");
        val1 = angka1 + val1;
    }
    Console.WriteLine();
    Console.WriteLine($"Jumlah value diagonal pertama : {val1}");

    Console.WriteLine("Diagonal 2 :");
    int val2 = 0;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (i == (2 - j))
            {
                Console.Write(array[i, j] + " ");
            }
            else
            {
                Console.Write("  ");
            }
        }
        Console.WriteLine();
    }
    for (int i = 0; i < 3; i++)
    {
        int angka2 = array[i, 2 - i];
        Console.Write(angka2 + " + ");
        val2 = val2 + angka2;
    }
    Console.WriteLine();
    Console.WriteLine($"Jumlah value diagonal kedua : {val2}");

    int total = val1 - val2;
    Console.WriteLine($"Perbedaan : {total}");

}

static void Nomor10()
{
    Console.WriteLine("Masukkan tinggi lilin, dipisahkan oleh spasi:");

    string input = Console.ReadLine();
    int[] arrTinggi = input.Split(' ').Select(int.Parse).ToArray();

    int maxTinggi = arrTinggi.Max();
    int count = 0;
     
    foreach (int n in arrTinggi)
    {
        if (n == maxTinggi)
        {
            count++;
        }
    }

    Console.WriteLine("Jumlah lilin yang berhasil ditiup: " + count);
}
