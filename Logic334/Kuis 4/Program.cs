﻿//Soal1();
//Soal2();
//Soal3();
//Soal4();
//Soal5();
//Soal6();
//Soal7();
//Soal8();
Console.ReadKey();


static void Soal1()
{
    Console.WriteLine("---Program menghitung gaji karyawan---");
    Console.Write("Golongan : ");
    int gol = int.Parse(Console.ReadLine());
    Console.Write("Jam Kerja : ");
    int jam = int.Parse(Console.ReadLine());
    int upah = 0;
    int total = 0;
    int perjam = 0;
    int jamLembur = 0;
    int lembur = 0;

    if (gol == 1)
    {
        perjam = 2000;
    }
    else if (gol == 2)
    {
        perjam = 3000;
    }
    else if (gol == 3)
    {
        perjam = 4000;
    }
    else if (gol == 4)
    {
        perjam = 5000;
    }
    else
    {
        perjam = 0;
    }


    if (jam <= 40)
    {
        upah = perjam * jam;
        Console.WriteLine($"Upah    : {upah}");
        Console.WriteLine("Lembur   : 0");
        Console.WriteLine($"Total : {upah}");
    }
    else
    {
        upah = perjam * 40;
        jamLembur = jam - 40;
        lembur = (jamLembur * (perjam + (perjam / 2)));
        total = upah + lembur;
        Console.WriteLine($"Upah    : {upah}");
        Console.WriteLine($"Lembur   : {lembur}");
        Console.WriteLine($"Total : {total}");
    }
}

static void Soal2()
{
    Console.WriteLine("---Menghitung jumlah kata---");
    Console.Write("Masukan kalimat (dipisahkan oleh 'spasi') : ");
    string[] input = Console.ReadLine().Split(" ");
    int count = 0;

    for (int i = 0; i < input.Length; i++)
    {
        Console.WriteLine($"Kata {i} = {input[i]}");
        count = count + 1;
    }
    Console.WriteLine($"Total kata adalah : {count}");
}

static void Soal3()
{
    Console.WriteLine("---Mengubah karakter ditengah kata menjadi *---");
    Console.Write("Masukan kalimat (dipisahkan oleh 'spasi') : ");
    string[] input = Console.ReadLine().Split(" ");

    for (int i = 0; i < input.Length; i++)
    {
        for (int j = 0; j < input[i].Length; j++)
        {
            if (j == 0 || j == input[i].Length-1)
            {
                Console.Write((char)input[i][j]);
            }
            else
                Console.Write("*");
        }
        Console.Write(" ");
    }
}

static void Soal4()
{
    Console.WriteLine("---Mengubah karakter awal dan akhir kata menjadi *---");
    Console.Write("Masukan kalimat (dipisahkan oleh 'spasi') : ");
    string[] input = Console.ReadLine().Split(" ");

    for (int i = 0; i < input.Length; i++)
    {
        for (int j = 0; j < input[i].Length; j++)
        {
            if (j == 0 || j == input[i].Length - 1)
            {
                Console.Write("*");
            }
            else
                Console.Write((char)input[i][j]);
        }
        Console.Write(" ");
    }
}

static void Soal5()
{
    Console.WriteLine("---Remove karakter awal pada kata ---");
    Console.Write("Masukan kalimat (dipisahkan oleh 'spasi') : ");
    string[] input = Console.ReadLine().Split(" "); 

    for (int i = 0; i < input.Length; i++)
    {
        for (int j = 0; j < input[i].Length; j++)
        {
            if (j == 0)
            {
                Console.Write("");
            }
            else
                Console.Write((char)input[i][j]);
        }
        Console.Write(" ");
    }
}

static void Soal6()
{
    Console.WriteLine("---Program menghitung pangkat dari sebuah bilangan sebanyak n---");
    Console.WriteLine("---Diganti menjadi '*' setiap pangkat (n) termasuk bilangan genap---");
    Console.Write("Masukan angka : ");
    int bil = int.Parse(Console.ReadLine());
    Console.Write("Dipangkatkan sejumlah (n) : ");
    int n = int.Parse(Console.ReadLine());
    int count = 1;
    for (int i = 0;i < n; i++)
    {
        count = bil * count;
        
        if(i % 2 == 0)
        {
            Console.Write($"{count} ");

        }
        else
            Console.Write("* ");
    }
}



static void Soal7(){
    Console.WriteLine("---Program membuat deret fibonacci---");
    Console.Write("Masukan jumlah bilangan : ");
    int n = int.Parse(Console.ReadLine());
    int val = 0;
    int bil0 = 0;
    int bil1 = 1;
   
    if(n == 0)
    {
        Console.Write("0");
    }
    else if (n == 1)
    {
        Console.Write(bil1);
    }
    else
    {
        for (int i = 0; i < n; i++)
        {
            bil0 = bil1;
            bil1 = val;
            val = bil0 + bil1;
            Console.Write(val);
            if (i != n-1)
            {
                Console.Write(", ");

            }
        }
    }
}

static void Soal8()
{
    Console.WriteLine("------");
    Console.Write("Masukan bilangan : ");
    int bil = int.Parse(Console.ReadLine());
    int n = bil;

    for (int i = 1; i <= bil; i++)
    {
        for (int j = 1; j <= bil; j++)
        {
            if (i == 1)
            {
                Console.Write($" {j}");
            }
            else if (i == bil && j == 1)
            {
                Console.Write($" {bil}");
            }
            else if (i == bil)
            {
                Console.Write($" {n = n - 1}");
            }
            else if (i > 1 && j == 1 || j == n)
            {
                Console.Write(" *");

            }
            else
            {
                Console.Write("  ");
            }
        }
        Console.WriteLine();
    }
}