﻿using System;

Tugas1();
//Tugas2();
//Tugas3();

Console.ReadKey();

static void Tugas1()
{
    double k, l = 0;
    const double phi = 3.14;
    Console.WriteLine("---Program menghitung keliling dan luas lingkaran---");
    Console.Write("Masukan jari-jari lingkaran : ");
    int r = int.Parse(Console.ReadLine());

    k = 2 * phi * r;
    int klg = Convert.ToInt32(k);
    Console.WriteLine($"Keliling dari lingkaran adalah = {klg} cm");

    l = phi * r * r;
    int ls = Convert.ToInt32(l);
    Console.WriteLine($"Luas dari lingkaran adalah = {ls} cm2");
}

static void Tugas2()
{
    int k, l = 0;
    Console.WriteLine("---Program menghitung luas dan keliling persegi---");
    Console.Write("Masukan sisi persegi : ");
    int s = int.Parse(Console.ReadLine());

    l = s * s;
    Console.WriteLine($"Luas dari persegi adalah adalah = {l} cm2");

    k = 4 * s;
    Console.WriteLine($"Keliling dari persegi adalah = {k} cm");
}

static void Tugas3()
{
    int h;
    Console.WriteLine("---Program mencari hasil modulus---");
    Console.Write("Masukan angka : ");
    int a = int.Parse(Console.ReadLine());
    Console.Write("Masukan pembagi : ");
    int p = int.Parse(Console.ReadLine());

    h = a % p;

    if (h == 0)
    {
        Console.WriteLine($"Angka {a} % {p} adalah 0");
    }
    else
        Console.WriteLine($"Angka {a} % {p} bukan 0 melainkan {h}");
}