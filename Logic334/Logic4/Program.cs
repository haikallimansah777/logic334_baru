﻿using Logic4;
using System.Linq.Expressions;

//InitializeArray();
//MengaksesElementArray();
//Array2Dimensi();
//InitializeList();
//PanggilClassStudent();
//MengaksesElementList();
//InsertList();
//IndexElementList();
//InitializeDateTime();
//ParsingDateTime();
//DateTimeProperties();
TimeSpan();

Console.ReadKey();
static void InitializeArray()
{
    Console.WriteLine("---Initialize Array---");

    //cara1
    int[] array = new int[5];

    //cara2
    int[] array2 = new int[5] { 1, 2, 3, 4, 5 };

    //cara3
    int[] array3 = new int[] { 1, 2, 3, 4, 5 };

    //cara4
    int[] array4 = { 1, 2, 3, 4, 5 };

    //cara5
    int[] array5;
    array5 = new int[] { 1, 2, 3, 4, 5 };

    Console.WriteLine(string.Join(" , ", array));
    Console.WriteLine(string.Join(" , ", array2));
    Console.WriteLine(string.Join(" , ", array3));
    Console.WriteLine(string.Join(" , ", array4));
    Console.WriteLine(string.Join(" , ", array5));
}

static void MengaksesElementArray()
{
    Console.WriteLine("---Menagkses elemen array---");

    int[] intArray = new int[3];
    //mengisi value array
    intArray[0] = 1;
    intArray[1] = 2;
    intArray[2] = 3;

    //get data array manual
    Console.WriteLine(intArray[0]);


    int[] array = { 101, 102, 103 };
    //get data with loop
    for (int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }


    string[] stringArray = { "A", "B", "C", "D", "E", "F", };
    //get data string array with foreach
    foreach (string str in stringArray)
    {
        Console.WriteLine(str);
    }
}

static void Array2Dimensi()
{
    Console.WriteLine("---Array 2 Dimensi---");

    int[,] array = new int[3, 3]
    {
        {1,2,3},
        {4,5,6},
        {7,8,9}
    };

    for (int i = 0; i < array.GetLength(0); i++)
    {
        for (int j = 0; j < array.GetLength(1); j++)
        {
            Console.Write(array[i, j] + " ");
        }
        Console.WriteLine();
    }
}

static void InitializeList()
{
    Console.WriteLine("---InitializeList---");

    List<string> buah = new List<string>()
    {
        "Apel",
        "Mangga",
        "Jeruk"
    };

    //tambahdata
    buah.Add("Melon");

    //ubahdata
    buah[0] = "Jambu";

    //remove data by value (meskipun tidak ada value yang sama = tidak akan error)
    buah.Remove("Jambu");

    //remove data by index (kalo index tidak ada = error)
    //buah.RemoveAt(0);

    Console.WriteLine(string.Join(",", buah));
}

static void PanggilClassStudent()
{
    Console.WriteLine("---Panggil classs student---");
    //inisialisasi class (jangan lupa pake using di atas buat reference)
   // Student student = new Student();

    //input statis
    List<Student> Kelas1 = new List<Student>()
    {
        new Student(){Id = 1, Name = "Haikal"},
        new Student(){Id = 2, Name = "Limansah"},
        new Student(){Id = 3, Name = "Leonardo"}
    };

    //tambah data
    Student student = new Student();
    student.Id = 4;
    student.Name = "De Caprio";
    Kelas1.Add(student);

    Kelas1.Add(new Student() { Id = 5, Name = "Rayyanza"});

    //hitung list
    Console.WriteLine($"Panjang data list student : {Kelas1.Count}");

    //loop class foreach show list
    foreach (Student varbebas in Kelas1)
    {
        Console.WriteLine($"Id : {varbebas.Id}, Name : {varbebas.Name}");
    }

    Console.WriteLine();
    //loop class for
    for (int i = 0; i < Kelas1.Count; i++)
    {
        Console.WriteLine($"Id : {Kelas1[i].Id}, Name : {Kelas1[i].Name}");
    }
}

static void MengaksesElementList()
{
    Console.WriteLine("---Mengakses element list---");

    List<int> angka = new List<int>();
    angka.Add(1);
    angka.Add(2);
    angka.Add(3);

    Console.WriteLine(angka[0]);
    Console.WriteLine(angka[1]);
    Console.WriteLine(angka[2]);

    Console.WriteLine();

    foreach (var item in angka)
    {
        Console.WriteLine(item);
    }

    Console.WriteLine();

    for (int i = 0; i< angka.Count; i++)
    {
        Console.WriteLine(angka[i]);
    }
}

static void InsertList()
{
    Console.WriteLine("---Insert List---");

    List<int> angka = new List<int>();
    angka.Add(1);
    angka.Add(2);
    angka.Add(3);

    angka.Insert(1, 4);

    for (int i = 0; i < angka.Count; i++)
    {
        Console.WriteLine(angka[i]);
    }
}

static void IndexElementList()
{
    Console.WriteLine("---Index Element List---");
    List<string> coba = new List<string>();
    coba.Add("a");
    coba.Add("b");
    coba.Add("c");

    Console.Write("Masukan huruf : ");
    string item = Console.ReadLine();

    int index = coba.IndexOf(item);

    if (index != -1)
    {   
        Console.WriteLine($"Element {item} is found at index {index}");
    }
    else
    {
        Console.WriteLine($"Element {item} not found");
    }
    
}

static void InitializeDateTime()
{
    Console.WriteLine("---Initialize DateTime---");

    DateTime dt1 = new DateTime(); //01/01/0001 00:00:00.000
    Console.WriteLine(dt1);

    DateTime dtNow = DateTime.Now; //tanggal dan waktu hari ini
    Console.WriteLine(dtNow);
    
    DateTime dtNow2 = DateTime.Now; //tanggal dan waktu hari ini
    Console.WriteLine(dtNow.ToString("dddd, yyyy/MMMM/dd")); //oprek di dd/mm/yyyy bisa diubah ubah tergantung kebutuhan

    DateTime dt2 = new DateTime(2023, 11, 1);
    Console.WriteLine(dt2);

    DateTime dt3 = new DateTime(2023, 11, 1, 10, 40, 25);
    Console.WriteLine(dt3);
}

static void ParsingDateTime()
{
    Console.WriteLine("---Parsing Date Time---");
    Console.Write("Masukan Date Time (dd/MM/yyyy) :  ");
    string dateString = Console.ReadLine();
    try
    {
        DateTime date1 = DateTime.ParseExact(dateString, "d/MM/yyyy", null);
        Console.WriteLine(date1);
    }
    catch (Exception ex)
    {
        Console.WriteLine("Format yang anda masukan salah");
        Console.WriteLine("Pesan error : "+ ex.Message);
    }
  
}

static void DateTimeProperties()
{
    Console.WriteLine("---DateTime Properties---");
    DateTime date = new DateTime(2023, 11, 1, 11, 10, 25);

    int tahun = date.Year;
    int bulan = date.Month;
    int hari = date.Day;
    int jam = date.Hour;
    int menit = date.Minute;
    int detik = date.Second;
    int weekday = (int)date.DayOfWeek;
    string hariString = date.DayOfWeek.ToString();
    string hariString2 = date.ToString("dddd");

    Console.WriteLine($"Tahun : {tahun}");
    Console.WriteLine($"Bulan : {bulan}");
    Console.WriteLine($"Hari : {hari}");
    Console.WriteLine($"Jam : {jam}");
    Console.WriteLine($"Menit : {menit}");
    Console.WriteLine($"Detik : {detik}");
    Console.WriteLine($"Weekday : {weekday}");
    Console.WriteLine($"HariString : {hariString}");
    Console.WriteLine($"HariString2 : {hariString2}");
}

static void TimeSpan()
{
    Console.WriteLine("---Timespan---");
    DateTime date1 = new DateTime(2016, 1, 10, 11, 20, 30);
    DateTime date2 = new DateTime(2016, 2, 20, 12, 25, 35);

    //Calculate the interval between the two date
    TimeSpan interval = date2 - date1;
    Console.WriteLine("Nomor of Days : " + interval.Days);
    Console.WriteLine("Total nomor of Days : " + interval.TotalDays);
    Console.WriteLine("Nomor of Hours : " + interval.Hours);
    Console.WriteLine("Total Nomor of Hours : " + interval.TotalHours);
    Console.WriteLine("Nomor of Minutes : " + interval.Minutes);
    Console.WriteLine("Total Nomor of Minutes : " + interval.Minutes);
    Console.WriteLine("Nomor of Seconds : " + interval.Seconds);
    Console.WriteLine("Total Nomor of Seconds : " + interval.TotalSeconds);
    Console.WriteLine("Nomor of Milliseconds : " + interval.Milliseconds);
    Console.WriteLine("Total Nomor of Milliseconds : " + interval.TotalMilliseconds);
    Console.WriteLine("Ticks : "+interval.Ticks);

}
