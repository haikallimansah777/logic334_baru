﻿//Soal1();
//Soal2();
//Soal3();
using System.Collections;

//Soal4();
//Soal5();
//Soal6();
//Console.ReadKey();

static void Soal1()
{
    Console.WriteLine("---Deret tiap suku ganjil menjadi (-)---");
    Console.Write("Masukan angka pertama  : ");
    int a = int.Parse(Console.ReadLine());
    Console.Write("Masukan pola pertambahan : ");
    int p = int.Parse(Console.ReadLine());
    Console.Write("Masukan jumlah suku : ");
    int n = int.Parse(Console.ReadLine());
    int tmp;
    tmp = a;

    Console.Write($"{a}");
    for (int i = 0; i < n - 1; i++)
    {
        if (a >= 0)
        {
                    tmp = (tmp + p);

        if (tmp % 2 == 0)
        {
            Console.Write($", -{tmp}");
        }
        else
        {
            Console.Write($", {tmp}");
        }
        }


    }
}

static void Soal2()
{
    Console.WriteLine("---Convert Jam AM/PM---");
    Console.Write("Input jam (HH:mm:ssTT) :  ");
    string jam = Console.ReadLine();
    DateTime dt = Convert.ToDateTime(jam);
    try
    {
        String hasil = dt.ToString("HH:mm:ss");
        Console.Write(hasil);
    }
    catch (Exception ex)
    {
        Console.WriteLine("Format yang anda masukan salah");
        Console.WriteLine("Pesan error : " + ex.Message);
    }

}

static void Soal3()
{
    Console.WriteLine("---Menentukan harga baju---");
    Console.Write("Masukan kode baju : ");
    int kode = int.Parse(Console.ReadLine());
    Console.Write("Masukan kode ukuran : ");
    string ukuran = Console.ReadLine().ToUpper();
    string merk;
    int harga;
    
    if (kode == 1)
    {
        merk = "IMP";
        if (ukuran == "S")
        {
            harga = 200000;
        }
        else if (ukuran == "M")
        {
            harga = 220000;
        }
        else
        {
            harga = 250000;
        }
        Console.WriteLine($"Merk baju = {merk}");
        Console.WriteLine($"Harga = {harga}");
    }
    else if (kode == 2)
    {
        merk = "Prada";
        if (ukuran == "S")
        {
            harga = 150000;
        }
        else if (ukuran == "M")
        {
            harga = 160000;
        }
        else
        {
            harga = 170000;
        }
        Console.WriteLine($"Merk baju = {merk}");
        Console.WriteLine($"Harga = {harga}");
    }
    else if (kode == 3)
    {
        merk = "Gucci";
        harga = 200000;
        Console.WriteLine($"Merk baju = {merk}");
        Console.WriteLine($"Harga = {harga}");
    }
    else
    {
        Console.WriteLine("Kode tidak sesuai");
    }
}

static void Soal4()
{
    Console.WriteLine("---Belanja Lebaran---");
    Console.Write("Masukan jumlah uang anda : ");
    int val = int.Parse(Console.ReadLine());
    Console.Write("Masukan harga baju (dipisahkan oleh ',') : ");
    string[] a = Console.ReadLine().Split(",");
    int[] baju = Array.ConvertAll(a, int.Parse);
    Console.Write("Masukan harga celana (dipisahkan oleh ',') : ");
    string[] b = Console.ReadLine().Split(",");
    int[] celana = Array.ConvertAll(b, int.Parse);
    int total = 0;
    int max = 0;

    if (baju.Length != celana.Length)
    {
        Console.WriteLine("Jumlah kategori baju dan celana tidak sama");
    }
    else 
    {
        for (int i = 0; i < baju.Length; i++)
        {
            for (int j = 0; j < celana.Length; j++)
            {
                total = celana[i] + baju[j];
                if (val >= total)
                {
                    max = Math.Max(max, total);
                }
            }
        }
        Console.Write(max);
    }
}

static void Soal5(){
    Console.WriteLine("---Pergeserakan bilangan dalam deret---");
    Console.Write("Masukan angka (dipisahkan oleh ',') : ");
    string[] input = Console.ReadLine().Split(",");
    int[] inputs = Array.ConvertAll(input, int.Parse);
    Console.Write("Digeser sebanyak : ");
    int n = int.Parse(Console.ReadLine());
    int tmp;

    for (int a = 1; a <= n; a++)
    {
        Console.Write($"Deret ke-{a} : ");
        tmp = inputs[0];
        for (int b = 0; b < inputs.Length - 1; b++)
        {
            inputs[b] = inputs[b + 1];
        }
        inputs[inputs.Length-1] = tmp;

        for (int c = 0; c < inputs.Length; c++)
        {
            Console.Write($"{inputs[c]}, ");
        }
        Console.WriteLine();
    }
}

static void Soal6()
{
//pake fungsi
/*    Console.WriteLine("---Sorting---");
    Console.Write("Masukan angka (dipisahkan oleh ',') : ");
    string[] input = Console.ReadLine().Split(",");
    int[] inputs = Array.ConvertAll(input, int.Parse);

    Array.Sort(inputs);

    Console.Write($"{inputs[0]}");
    for (int i = 1; i < inputs.Length; i++)
    {
        Console.Write($",{inputs[i]}");
    }*/

//bubble sorting
    Console.WriteLine("---Sorting---");
    Console.Write("Masukan angka (dipisahkan oleh ',') : ");
    string[] input = Console.ReadLine().Split(",");
    int[] deret = Array.ConvertAll(input, int.Parse);
    int tmp = 0;

    for (int i = 0; i < deret.Length - 1; i++)
    {
        for (int j = 0; j < deret.Length - i - 1; j++) 
        {
            if (deret[j] > deret[j + 1])
            {
                tmp = deret[j+1];
                deret[j+1] = deret[j];
                deret[j] = tmp;
            }
        }
    }

    Console.Write($"{deret[0]}");
    for (int k = 1; k < deret.Length; k++)
    {
        Console.Write($",{deret[k]}");
    }


}




//1 dim: Practice
//Soal1sampai4();
//Soal5kedua();
Console.ReadKey();

static void Soal1sampai4()
{
    Console.WriteLine("---Deret---");
    Console.Write("Masukan angka pertama  : ");
    int a = int.Parse(Console.ReadLine());
    Console.Write("Masukan pola pertambahan : ");
    int p = int.Parse(Console.ReadLine());
    Console.Write("Masukan jumlah suku : ");
    int n = int.Parse(Console.ReadLine());
    int tmp;
    tmp = a;

    Console.Write($"{a}");
    for (int i = 0; i < n-1; i++)
    {
        tmp = (tmp + p);
        Console.Write($", {tmp}");
    }
}

static void Soal5kedua(){
    Console.WriteLine("---Deret diberi * tiap 2 suku---");
    Console.Write("Masukan angka pertama  : ");
    int a = int.Parse(Console.ReadLine());
    Console.Write("Masukan pola pertambahan : ");
    int p = int.Parse(Console.ReadLine());
    Console.Write("Masukan jumlah suku : ");
    int n = int.Parse(Console.ReadLine());
    int tmp;
    tmp = a;

    Console.Write($"{a}");
    for (int i = 1; i < n; i++)
    {
        if(((i+1) % 3) == 0)
        {
            Console.Write(", *");
        }
        else
        {
            tmp = (tmp + p);
            Console.Write($", {tmp}");
        }
    }

}

