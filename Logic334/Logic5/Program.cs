﻿//PadLeft();
//Rekursif();
Console.ReadKey();

static void PadLeft()
{
    Console.WriteLine("---Pad Left---");
    Console.Write("Masukan input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukan panjang karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write("Masukan char : ");
    char chars = char.Parse(Console.ReadLine());

    //231100001 (2 digit pertama tahun), (2 digit selanjutnya tahun) sisanya generate length

    DateTime date = DateTime.Now;

    string code = "";

    code = date.ToString("yyMM") + input.ToString().PadLeft(panjang, chars);
//    code = date.ToString("yyMM") + input.ToString().PadRight(panjang, chars);
    Console.WriteLine($"Hasil Padleft : {code}");
}

static void Rekursif()
{
    Console.WriteLine("---Fungsi Rekursif---");
    Console.Write("Masukan input awal : ");
    int awal = int.Parse(Console.ReadLine());
    Console.Write("Masukan input akhir integer n : ");
    int akhir = int.Parse(Console.ReadLine());
/*    Console.Write("Masukan tipe (asc/desc) : ");
    string tipe = Console.ReadLine();*/

    //memanggil fungsi desc
    //Perulangan1(awal, akhir);

    //memanggil fungsi asc
    //Perulangan2(akhir, awal);
}

static int Perulangan1(int awal, int akhir)
{
    if (awal == akhir)
    {
        Console.WriteLine(akhir);
        return 0;
    }
    Console.WriteLine(akhir);
    return Perulangan1(awal, akhir-1); 
}

static int Perulangan2(int akhir, int awal)
{
    if (awal == akhir)
    {
        Console.WriteLine(awal);
        return 0;
    }
    Console.WriteLine(awal);
    return Perulangan2(akhir,awal +1);
}
