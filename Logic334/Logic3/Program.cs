﻿//bisa compile


//PerulanganWhile();
//PerulanganWhile2();
//PerulanganDoWhile();
//PerulanganFor();
//Break();
//Continue();
//ForNested();
//ForEach();
//Length();
//RemoveString();
//InsertString();
//ReplaceString();
//SplitAndJoin();
//SubString();
//ContainsString();
ToCharArray();
//ConvertAll();
Console.ReadKey();

static void PerulanganWhile()
{
    Console.WriteLine("---Perulangan While");
    Console.Write("Masukan nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    while (nilai < 6)
    {
        Console.WriteLine(nilai);
        nilai++;
    }
}

static void PerulanganWhile2()
{
    bool ulangi = true;

    Console.WriteLine("---Perulangan While 2---");
    Console.Write("Masukan nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    while (ulangi == true)
    {

        Console.WriteLine($"Proses ke {nilai}");
        nilai++;

        Console.WriteLine("Ulangi proses? (y/n)");
        string input = Console.ReadLine();

        if (input.ToLower() == "n")
        {
            ulangi = false;
        }
    }
}

static void PerulanganDoWhile()
{
    Console.WriteLine("---Perulangan DoWhile---");
    Console.Write("Masukan nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    do
    {
        Console.WriteLine(nilai);
        nilai++;
    } while (nilai < 6);
}

static void PerulanganFor()
{
    Console.WriteLine("---Perulangan For---");
    Console.Write("Masukan input : ");


    int input = int.Parse(Console.ReadLine());
    
    for(int i = 0; i < input; i++)
    {
        Console.Write
            (i + "\t");
    }

//    Console.WriteLine();
    Console.Write("\n");

    for (int i = input; i >= 0; i--)
    {
        Console.WriteLine(i);
    }
   
}

static void Break()
{
    for (int i = 0; i < 10; i++)
    {
        if (i == 6)
        {
            break;
        }
        Console.WriteLine(i);
    }

}static void Continue()
{
    for (int i = 0; i < 10; i++)
    {
        if (i == 6)
        {
            continue;
        }
        Console.WriteLine(i);
    }

}

static void ForNested()
{
    for(int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++) 
        {
            Console.Write($"({i},{j})");
        }
        Console.WriteLine();
    }
}


static void ForEach()
{
    int[] array = { 1, 2, 3, 4, 5 };
    int sum = 0;
   // int count = 0;

    foreach(int x in array)
    {
        sum += x;
    }
    Console.WriteLine($"Jumlah nilai : {sum}");

    Console.WriteLine();

    for (int i = 0;i < array.Length; i++)
    {
        sum += array[i];
    }
    Console.WriteLine($"Jumlah nilai : {sum}");

}

static void Length()
{
    //variabel.ToUpper = mengubah ke huruf besar semua
    //variabel.Length = menghitung jumlah karakter
    Console.WriteLine("---Length---");
    Console.Write("Masukan kata : ");
    string kata = Console.ReadLine();
    Console.WriteLine($"Kata : {kata.ToUpper()} mempunyai panjang karakter sebanyak = {kata.Length}");
}

static void RemoveString()
{
    Console.WriteLine("---Remove String---");
    Console.Write("Masukan kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukan index remove : ");
    int index = int.Parse(Console.ReadLine());
    Console.WriteLine($"Kata yang baru adalah : {kata.Remove(index)}");
}

static void InsertString()
{
    Console.WriteLine("---Remove String---");
    Console.Write("Masukan kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukan index insert : ");
    int index = int.Parse(Console.ReadLine());
    Console.Write("Masukan kata yang ingin disisipkan : ");
    string insert = Console.ReadLine();

    Console.WriteLine($"Kata yang baru adalah : {kata.Insert(index, insert)}");
}

static void ReplaceString()
{
    Console.WriteLine("---Replace String---");
    Console.Write("Masukan kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukan kata yang akan direplace : ");
    string kataLama = Console.ReadLine();
    Console.Write("Masukan kata yang baru: ");
    string kataBaru = Console.ReadLine();

    Console.WriteLine($"Kata yang baru adalah : {kata.Replace(kataLama, kataBaru)}");
}

static void SplitAndJoin()
{
    Console.WriteLine("Split and Join");
    Console.Write("Masukan kalimat : ");
    string kalimat = Console.ReadLine();
    Console.Write("Masukan split  : ");
    string split = Console.ReadLine();

    string[] katakata = kalimat.Split(split);

    foreach(string kata in katakata)
    {
        Console.WriteLine(kata);
    }

    Console.WriteLine(string.Join("+", katakata));

    Console.WriteLine();

    int[] deret = { 1, 2, 3, 4, 5 };
    Console.WriteLine(string.Join ("+", deret));
}

static void SubString()
{
    Console.WriteLine("---Sub String---");
    Console.Write("Masukan kode : ");
    string kode = Console.ReadLine();
    Console.Write("Masukan parameter 1 : ");
    int param1 = int.Parse(Console.ReadLine());
    Console.Write("Masukan parameter 2 : ");
    int param2 = int.Parse(Console.ReadLine());

    if (param2 == 0)
    {
        Console.WriteLine($"Hasil substring = {kode.Substring(param1)}");
    }
    else
    {
       Console.WriteLine($"Hasil substring = {kode.Substring(param1, param2)}");
    }
}   

static void ContainsString()
{
    Console.WriteLine("");
    Console.Write("Masukan kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukan contain : ");
    string contain = Console.ReadLine();

    if (kata.Contains(contain))
    {
        Console.WriteLine($"Kata {kata} mengandung {contain}");
    }
    else
    {
        Console.WriteLine($"Kata {kata} tidak mengandung mengandung {contain}");
    }
}

static void ToCharArray()
{
    Console.WriteLine("---String to Char Array---");
    Console.Write("Masukan kalimat : ");
    string kalimat = Console.ReadLine();

    char[] array = kalimat.ToCharArray();

    foreach (char c in array)
    {
        Console.WriteLine(c);
    }

    Console.WriteLine();

    for (int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }
}


static void convertAll()
{
    Console.WriteLine("Convert All");
    Console.Write("Masukkan Input Angka (Pisahkan Dengan Spasi): ");
    string[] input = Console.ReadLine().Split(" ");

    int jumlah = 0;
    int[] array = Array.ConvertAll(input, int.Parse);

    foreach (int i in array)
    {
        jumlah += i;
    }

    Console.WriteLine("jumlah : " + jumlah);


}

