﻿using Logic6;

//AbstractClass();
//ObjectClass();
//Encapsulation();
Inheritance();

Console.ReadKey();

static void AbstractClass()
{
    Console.WriteLine("---Abstract Class---");
    Console.Write("Masukan input x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukan input y : ");
    int y = int.Parse(Console.ReadLine());

    TestTurunan calc = new TestTurunan();
    int jumlah = calc.jumlah(x,y);
    int kurang = calc.kurang(x,y);

    Console.WriteLine($"Hasil {x} + {y} = {jumlah}");
    Console.WriteLine($"Hasil {x} - {y} = {kurang}");
}   

static void ObjectClass()
{
    Console.WriteLine("---Object Class---");

    Mobil mobil = new Mobil() { nama = "Ferrari", kecepatan = 0, bensin = 10, posisi = 0 };
    //bisa juga input class dengan cara gini
    //mobil.nama = "Ferrari";
    //mobil.kecepatan = 0; ---dan seterusnya---
    mobil.percepat();
    mobil.maju();
    mobil.isiBensin(20);
    mobil.utama();
    
}

static void Constructor()
{
    Console.WriteLine("---Constructor---");
    Mobil mobil = new Mobil("B 123 MX");
    string platno = mobil.getPlatno();

    Console.WriteLine($"Mobil dengan nomor polisi : {platno}");

}

static void Encapsulation()
{
    Console.WriteLine("---Encapsulation---");
    PersegiPanjang pp = new PersegiPanjang();
    pp.panjang = 4.5;
    pp.lebar = 3.5;

    pp.TampilkanData();
}

static void Inheritance()
{
    Console.WriteLine("---Inheritance---");
    TypeMobil typeMobil = new TypeMobil();
    typeMobil.Civic();

}

static void Overriding()
{
    Console.WriteLine("---Overriding---");
    Kucing kucing = new Kucing();
    Paus paus = new Paus();
    Console.WriteLine($"kucing {kucing.pindah()}");
    Console.WriteLine($"paus {paus.pindah}");
}
