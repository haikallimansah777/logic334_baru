﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic6
{
    public class Mobil
    {
        public double kecepatan;
        public double bensin;
        public double posisi;
        public string nama;
        public string platno;


        public void percepat()
        {
            this.kecepatan += 10;
            this.bensin -= 5;
        }

        public void maju()
        {
            this.posisi += this.kecepatan;
            this.bensin -= 2;
        }

        public void isiBensin(double bensin)
        {
            this.bensin += bensin;
        }


        //terakhir untuk menu utama, bikin atas2nya dulu
        public void utama()
        {
            Console.WriteLine($"Nama    = {nama}");
            Console.WriteLine($"Bensin = {bensin}");
            Console.WriteLine($"Plat no = {platno}");
            Console.WriteLine($"Kecepatan = {kecepatan}");
            Console.WriteLine($"Posisi = {posisi}");
        }

        //membuat constructor
        public Mobil(string _platno)
        {
            this.platno = _platno;
        }

        //constructor 2
        public Mobil() 
        {

        }

        public string getPlatno()
        {
            return platno;
        }

    }
}


