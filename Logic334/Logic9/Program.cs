﻿//FungsiMath();
//Cetak_aVeryBigSum();


Console.ReadKey();


static void Cetak_aVeryBigSum()
{
    List<long> list = new List<long>() { 1000000001, 1000000002, 1000000003, 1000000004, 1000000005 };
    long result = aVeryBigSum(list);
    Console.WriteLine($"Hasil aVeryBigSum = {result}");
}

static long aVeryBigSum(List<long> ar)
{
    long result = 0;
    result = Convert.ToInt64(ar.Where(a => a == 1000000001).OrderByDescending(a => a).Sum(a => a)); //contoh pakai Where & Sum
    return result;
}

static void FungsiMath()
{
    Console.WriteLine("--Fungsi Math--");
    Console.Write("Masukan input angka : ");
    decimal angka = decimal.Parse(Console.ReadLine());

    Console.WriteLine($"Hasil fungsi Abs : {Math.Abs(angka)}");
    Console.WriteLine($"Hasil fungsi Round : {Math.Round(angka)}");
    Console.WriteLine($"Hasil fungsi Round 2 digit decimal : {Math.Round(angka, 2)}");
    Console.WriteLine($"Hasil fungsi Round 2 digit decimal (bulat ke bawah) : {Math.Round(angka, 2, MidpointRounding.ToNegativeInfinity)}");
    Console.WriteLine($"Hasil fungsi Round 2 digit decimal (bulat ke atas) : {Math.Round(angka, 2, MidpointRounding.ToPositiveInfinity)}");
    Console.WriteLine($"Hasil fungsi Pow (pangkat) : {Math.Pow(Convert.ToDouble(angka), 2)}");
    Console.WriteLine($"Hasil fungsi Min : {Math.Min(1, 2)}");
    Console.WriteLine($"Hasil fungsi Max : {Math.Max(1, 2)}");

}