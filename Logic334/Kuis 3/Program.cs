﻿//bisa compile

Nomor1();
//Nomor2();
//Nomor3();
//Nomor4();
//Nomor5();
//Nomor6();
//Nomor7();
//Nomor8();
//Console.ReadKey();

static void Nomor1()
{
    Console.WriteLine("---Program grade nilai---");
    Console.Write("Masukan nilai : ");
    int n = int.Parse(Console.ReadLine());

    if (n >= 90 && n <= 100)
    {
        Console.WriteLine("Mendapatkan grade : A");
    }

    else if (n >= 70 && n <= 89)
    {
        Console.WriteLine("Mendapatkan grade : B");
    }
    else if (n >= 50 && n <= 69)
    {
        Console.WriteLine("Mendapatkan grade : C");
    }
    else if (n <= 49)
    {
        Console.WriteLine("Mendapatkan grade : E");
    }
}


static void Nomor2()
{
    Console.WriteLine("--Program menghitung poin berdasarkan pembelian pulsa---");
    Console.Write("Pulsa : ");
    int pulsa = int.Parse(Console.ReadLine());
    if(pulsa >= 0 && pulsa < 10000)
    {
        Console.WriteLine("Point : 0");
    }
    else if (pulsa >= 10000 && pulsa < 25000)
    {
        Console.WriteLine("Point : 80");
    }
    else if (pulsa >= 25000 && pulsa < 50000)
    {
        Console.WriteLine("Point : 200");
    }
    else if (pulsa >= 50000 && pulsa < 100000)
    {
        Console.WriteLine("Point : 400");
    }
    else if (pulsa >= 100000)
    {
        Console.WriteLine("Point : 800");
    }

}

static void Nomor3()
{
    Console.WriteLine("--Program menghitung total belanja---");
    Console.Write("Belanja : ");
    int belanja = int.Parse(Console.ReadLine());
    Console.Write("Jarak : ");
    int jarak = int.Parse(Console.ReadLine());
    Console.Write("Masukan Promo : ");
    string promo = Console.ReadLine().ToUpper();

    double diskon, total = 0;
    int ongkir = 0;

    if (jarak >= 0 && jarak <= 5)
        ongkir = 5000;
    else if (jarak > 5)
        ongkir = (5000) + ((jarak - 5) * (1000));
    
    if (belanja >= 30000)
    {
        switch (promo)
        {
            case "JKTOVO":
                Console.WriteLine("Anda berhak mendapatkan diskon 40%");
                Console.WriteLine($"Belanja : {belanja}");
                diskon = (belanja * 0.4);
                if(diskon > 30000)
                {
                    diskon = 30000;
                }
                Console.WriteLine($"Diskon 40% : {diskon}");
                Console.WriteLine($"Ongkir : {ongkir}");
                total = (belanja - diskon)+ongkir;
                Console.WriteLine($"Total Belanja : {total}");
                break;
            default:
                Console.WriteLine("Kode promo salah");
                break;
        }
    }
    else
    {
        Console.WriteLine($"Belanja : {belanja}");
        Console.WriteLine("Diskon  : 0");
        Console.WriteLine($"Ongkir : {ongkir}");
        total = (belanja + ongkir);
        Console.WriteLine($"Total Belanja : {total}");

    }
}

static void Nomor4()
{
    Console.WriteLine("---Program menghitung total belanja dan gratis ongkir---");
    Console.WriteLine("1. Min Order 30rb = diskon ongkir 5rb dan diskon belanja 5rb");
    Console.WriteLine("2. Min Order 50rb = diskon ongkir 10rb dan diskon belanja 10rb");
    Console.WriteLine("3. Min Order 100rb = diskon ongkir 20rb dan diskon belanja 10rb");
    Console.Write("Belanja : ");
    int belanja = int.Parse(Console.ReadLine());
    Console.Write("Ongkos kirim : ");
    int ongkir = int.Parse(Console.ReadLine());
    Console.Write("Pilih voucher : ");
    string promo = Console.ReadLine();


    if (belanja >= 30000 && belanja < 50000)
    {
        switch (promo)
        {
            case "1":
                int total = 0;
                int dBelanja = 5000;
                int dOngkir = 5000;
                Console.WriteLine("Anda berhak mendapatkan potongan ongkir 5rb dan potongan harga belanja 5rb");
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos kirim : {ongkir}");
                Console.WriteLine($"Diskon ongkir : {dOngkir}");
                Console.WriteLine($"Diskon belanja : {dBelanja}");
                if (dOngkir > ongkir)
                {
                    ongkir = 0;
                }
                else
                {
                    ongkir = ongkir - dOngkir;
                }
                belanja = belanja - dBelanja;
                total = belanja + ongkir;
                Console.WriteLine($"Total belanja : {total}");
                break;
            case "2":
                Console.WriteLine("Anda tidak dapat menggunakan voucher ini");
                break;
            case "3":
                Console.WriteLine("Anda tidak dapat menggunakan voucher ini");
                break;
        }
    }
    else if (belanja >= 50000 && belanja < 100000)
    {
        switch (promo)
        {
            case "1":
                int total = 0;
                int dBelanja = 5000;
                int dOngkir = 5000;
                Console.WriteLine("Anda berhak mendapatkan potongan ongkir 5rb dan potongan harga belanja 5rb");
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos kirim : {ongkir}");
                Console.WriteLine($"Diskon ongkir : {dOngkir}");
                Console.WriteLine($"Diskon belanja : {dBelanja}");
                if (dOngkir > ongkir)
                {
                    ongkir = 0;
                }
                else
                {
                    ongkir = ongkir - dOngkir;
                }
                belanja = belanja - dBelanja;
                total = belanja + ongkir;
                Console.WriteLine($"Total belanja : {total}");
                break;
            case "2":
                int total2 = 0;
                int dBelanja2 = 10000;
                int dOngkir2 = 10000;
                Console.WriteLine("Anda berhak mendapatkan potongan ongkir 10rb dan potongan harga belanja 10rb");
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos kirim : {ongkir}");
                Console.WriteLine($"Diskon ongkir : {dOngkir2}");
                Console.WriteLine($"Diskon belanja : {dBelanja2}");
                if (dOngkir2 > ongkir)
                {
                    ongkir = 0;
                }
                else
                {
                    ongkir = ongkir - dOngkir2;
                }
                belanja = belanja - dBelanja2;
                total2 = belanja + ongkir;
                Console.WriteLine($"Total belanja : {total2}");
                break;
            case "3":
                Console.WriteLine("Anda tidak dapat menggunakan voucher ini");
                break;
        }
    }
    else if (belanja >= 100000)
    {
        switch (promo)
        {
            case "1":
                int total = 0;
                int dBelanja = 5000;
                int dOngkir = 5000;
                Console.WriteLine("Anda berhak mendapatkan potongan ongkir 5rb dan potongan harga belanja 5rb");
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos kirim : {ongkir}");
                Console.WriteLine($"Diskon ongkir : {dOngkir}");
                Console.WriteLine($"Diskon belanja : {dBelanja}");
                if (dOngkir > ongkir)
                {
                    ongkir = 0;
                }
                else
                {
                    ongkir = ongkir - dOngkir;
                }
                belanja = belanja - dBelanja;
                total = belanja + ongkir;
                Console.WriteLine($"Total belanja : {total}");
                break;
            case "2":
                int total2 = 0;
                int dBelanja2 = 10000;
                int dOngkir2 = 10000;
                Console.WriteLine("Anda berhak mendapatkan potongan ongkir 10rb dan potongan harga belanja 10rb");
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos kirim : {ongkir}");
                Console.WriteLine($"Diskon ongkir : {dOngkir2}");
                Console.WriteLine($"Diskon belanja : {dBelanja2}");
                if (dOngkir2 > ongkir)
                {
                    ongkir = 0;
                }
                else
                {
                    ongkir = ongkir - dOngkir2;
                }
                belanja = belanja - dBelanja2;
                total2 = belanja + ongkir;
                Console.WriteLine($"Total belanja : {total2}");
                break;
            case "3":
                int total3 = 0;
                int dBelanja3 = 10000;
                int dOngkir3 = 10000;
                Console.WriteLine("Anda berhak mendapatkan potongan ongkir 20rb dan potongan harga belanja 10rb");
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos kirim : {ongkir}");
                Console.WriteLine($"Diskon ongkir : {dOngkir3}");
                Console.WriteLine($"Diskon belanja : {dBelanja3}");
                if (dOngkir3 > ongkir)
                {
                    ongkir = 0;
                }
                else
                {
                    ongkir = ongkir - dOngkir3;
                }
                belanja = belanja - dBelanja3;
                total3 = belanja + ongkir;
                Console.WriteLine($"Total belanja : {total3}");
                break;
        }
    }
    else if (belanja < 30000)
    {
        Console.WriteLine($"Belanja : {belanja}");
        Console.WriteLine($"Ongkos kirim : {ongkir}");
        Console.WriteLine("Diskon ongkir : 0");
        Console.WriteLine("Diskon belanja : 0");
        int total0 = 0;
        total0 = belanja + ongkir;
        Console.WriteLine($"Total belanja : {total0}");
    }
}

static void Nomor5()
{
    Console.WriteLine("---Program nama dan generasi---");
    Console.Write("Masukan nama anda : ");
    string nama = Console.ReadLine();
    Console.Write("Tahun berapa anda lahir : ");
    int tahun = int.Parse(Console.ReadLine());

    if (tahun >= 1944 && tahun <= 1964)
    {
        Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong baby boomer");
    }
    else if (tahun >= 1965 && tahun <= 1979)
    {
        Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi X");
    }
    else if (tahun >= 1980 && tahun <= 1994)
    {
        Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Y (Millenials)");
    }
    else if (tahun >= 1995 && tahun <= 2015)
    {
        Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Z");
    }
    else
    {
        Console.WriteLine($"{nama}, berdasarkan tahun lahir anda, generasi anda tidak ada istilahnya");
    }

}

static void Nomor6()
{
    Console.WriteLine("---Program menghitung total gaji---");
    Console.Write("Nama : ");
    string nama = Console.ReadLine();
    Console.Write("Tunjangan : ");
    int tunjangan = int.Parse(Console.ReadLine());
    Console.Write("Gapok : ");
    int gapok = int.Parse(Console.ReadLine());
    Console.Write("Banyak bulan : ");
    int bulan = int.Parse(Console.ReadLine());
    double tmp = 0;
    double pajak = 0;
    tmp = (gapok + tunjangan);

    if (tmp <= 5000000)
    {
        pajak = 0.05;
    }
    else if (tmp >= 5000000 && tmp <= 10000000)
    {
        pajak = 0.1;
    }
    else if (tmp > 10000000)
    {
        pajak = 0.15;
    }

    double bpjs = 0;
    double totalPajak = 0;
    double totalGaji = 0;
    double gajiBulan = 0;
    totalPajak = tmp * pajak;
    gajiBulan = (tmp - (totalPajak + bpjs));
    totalGaji = (tmp - (totalPajak+bpjs))*bulan;

    Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut : ");
    Console.WriteLine($"Pajak                   :   Rp. {totalPajak}");
    Console.WriteLine($"Bpjs                    :   Rp. {bpjs}");
    Console.WriteLine($"Gaji/bulan              :   Rp. {gajiBulan}");
    Console.WriteLine($"Total gaji/banyak bulan :   Rp. {totalGaji}");
}
static void Nomor7()
{
    Console.WriteLine("---Program menghitung BMI---");
    Console.Write("Masukan berat badan anda (kg) : ");
    float berat = float.Parse(Console.ReadLine());
    Console.Write("Masukan tinggi badan anda (cm) : ");
    float tinggi = float.Parse(Console.ReadLine());
    float bmi,mtinggi = 0;
    mtinggi = tinggi / 100;
    bmi = berat / (mtinggi * mtinggi);
    Console.WriteLine($"Nilai BMI anda adalah {Math.Round(bmi,4)}");
    if (bmi < 18.5)
    {
        Console.WriteLine("Anda termasuk berbadan kurus");
    }
    else if (bmi >= 18.5 &&  bmi < 25)
    {
        Console.WriteLine("Anda termasuk berbadan langsing/sehat");
    }
    else if (bmi >= 25)
    {
        Console.WriteLine("Anda termasuk berbadan gemuk");
    }
}
static void Nomor8()
{
    Console.WriteLine("===Program menghitung nilai rata-rata===");
    Console.WriteLine("===Satuan nilai 0-100===");
    Console.Write("Masukan Nilai MTK : ");
    int mtk = int.Parse(Console.ReadLine());
    Console.Write("Masukan Nilai Fisika : ");
    int fisika = int.Parse(Console.ReadLine());
    Console.Write("Masukan Nilai Kimia : ");
    int kimia = int.Parse(Console.ReadLine());
    int avg = 0;
    avg = ((mtk+fisika+kimia)/3);
    Console.WriteLine($"Nilai Rata-Rata : {avg}");
    if (avg >= 50)
    {
        Console.WriteLine("Selamat");
        Console.WriteLine("Kamu Berhasil");
        Console.WriteLine("Kamu Hebat");
    }
    else
    {
        Console.WriteLine("Maaf");
        Console.WriteLine("Kamu Gagal");
    }
}

Soal3();
Console.ReadKey();
static void Soal3()
{
    Console.WriteLine("---String to Char Array---");
    Console.Write("Masukan kalimat : ");
    string[] input = Console.ReadLine().Split();

    for (int i = 0; i < input.Length; i++)
    {
        if (i == 0 && i == input.Length)
        {
            Console.Write("*");
        }
        else
            Console.Write(input[i]);
    }
}