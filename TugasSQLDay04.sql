---Create Database---
CREATE DATABASE DB_HR

--Create Table---
CREATE TABLE tb_karyawan(
	id bigint PRIMARY KEY identity (1,1) NOT NULL,
	nip varchar(50) NOT NULL,
	nama_depan varchar(50) NOT NULL,
	nama_belakang varchar(50) NOT NULL,
	jenis_kelamin varchar(50) NOT NULL,
	agama varchar(50) NOT NULL,
	tempat_lahir varchar(50) NOT NULL,
	tgl_lahir date NULL,
	alamat varchar(100) NOT NULL,
	pendidikan_terakhir varchar(50) NOT NULL,
	tgl_masuk date NULL
)

CREATE TABLE tb_divisi(
	id bigint PRIMARY KEY identity (1,1) NOT NULL,
	kd_divisi varchar(50) NOT NULL,
	nama_divisi varchar(50) NOT NULL
)

CREATE TABLE tb_jabatan(
	id bigint PRIMARY KEY identity (1,1) NOT NULL,
	kd_jabatan varchar(50) NOT NULL,
	nama_jabatan varchar(50) NOT NULL,
	gaji_pokok numeric NULL,
	tunjangan_jabatan numeric NULL
)

CREATE TABLE tb_pekerjaan(
	id bigint PRIMARY KEY identity (1,1) NOT NULL,
	nip varchar(50) NOT NULL,
	kode_jabatan varchar(50) NOT NULL,
	kode_divisi varchar(50) NOT NULL,
	tunjangan_kinerja numeric NULL,
	kota_penempatan varchar(50) NULL
)

--Insert Data
INSERT INTO tb_karyawan(nip, nama_depan, nama_belakang, jenis_kelamin, agama, tempat_lahir, tgl_lahir, alamat, pendidikan_terakhir, tgl_masuk)
VALUES
	('001','Hamidi','Samsudin','Pria','Islam','Sukabumi','1977-04-21','Jl.Sudirman No.12','S1 Teknik Mesin','2015-12-07'),
	('002','Ghandi','Wamida','Wanita','Islam','Palu','1992-01-12','Jl. Rambutan No.22','SMA Negeri 02 Palu','2014-12-01'),
	('003','Paul','Christian','Pria','Kristen','Ambon','1980-05-27','Jl. Veteran No.4','S1 Pendidikan Geografi','2014-01-12')

INSERT INTO tb_divisi(kd_divisi, nama_divisi)
VALUES
	('GD','Gudang'),
	('HRD','HRD'),
	('KU','Keuangan'),
	('UM','Umum')

INSERT INTO tb_jabatan(kd_jabatan, nama_jabatan, gaji_pokok, tunjangan_jabatan)
VALUES
	('MGR','Manager','5500000','1500000'),
	('OB','Office Boy','1900000','200000'),
	('ST','Staff','3000000','750000'),
	('WMGR','Wakil Manager','4000000','1200000')

INSERT INTO tb_pekerjaan(nip, kode_jabatan, kode_divisi, tunjangan_kinerja, kota_penempatan)
VALUES
	('001','ST','KU','750000','Cianjur'),
	('002','OB','UM','350000','Sukabumi'),
	('003','MGR','HRD','1500000','Sukabumi')

--1. Tampilkan nama lengkap, nama jabatan, tunjangan jabatan + gaji , yang gaji + tunjangan kinerja dibawah 5juta
SELECT CONCAT_WS(' ',nama_depan,nama_belakang) as nama_lengkap, nama_jabatan, tunjangan_jabatan + gaji_pokok as gaji_tunjangan
FROM tb_karyawan as k
JOIN tb_pekerjaan as p on k.nip = p.nip
JOIN tb_jabatan as j on p.kode_jabatan = j.kd_jabatan
WHERE gaji_pokok+tunjangan_kinerja < 5000000

--2. Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gendernya pria dan penempatan kerjanya diluar sukabumi
SELECT CONCAT_WS(' ',nama_depan,nama_belakang) as nama_lengkap, nama_jabatan, nama_divisi,
gaji_pokok+tunjangan_jabatan+tunjangan_kinerja as total_gaji,
(0.05)*(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja) as pajak,
(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja) - ((0.05)*(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)) as gaji_bersih
FROM tb_karyawan as k
JOIN tb_pekerjaan as p on k.nip = p.nip
JOIN tb_jabatan as j on p.kode_jabatan = j.kd_jabatan
JOIN tb_divisi as d on d.kd_divisi = p.kode_divisi
WHERE jenis_kelamin = 'PRIA' and kota_penempatan != 'SUKABUMI'

--3. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja) * 7
SELECT k.nip, CONCAT_WS(' ',nama_depan,nama_belakang) as nama_lengkap, nama_jabatan, nama_divisi,
((gaji_pokok+tunjangan_jabatan+tunjangan_kinerja) * 0.25) * 7 as bonus
FROM tb_karyawan as k
JOIN tb_pekerjaan as p on k.nip = p.nip
JOIN tb_jabatan as j on p.kode_jabatan = j.kd_jabatan
JOIN tb_divisi as d on d.kd_divisi = p.kode_divisi
order by nip asc

--4. Tampilkan nama lengkap, ttal gaji, infak(5%*total gaji) yang mempunyai jabatan MGR
SELECT k.nip,CONCAT_WS(' ',nama_depan,nama_belakang) as nama_lengkap, j.nama_jabatan, d.nama_divisi,
gaji_pokok+tunjangan_jabatan+tunjangan_kinerja as total_gaji,
(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*(0.05) as infak
FROM tb_karyawan as k
JOIN tb_pekerjaan as p on k.nip = p.nip
JOIN tb_jabatan as j on p.kode_jabatan = j.kd_jabatan
JOIN tb_divisi as d on d.kd_divisi = p.kode_divisi
WHERE kode_jabatan = 'MGR'

--5. Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1
SELECT k.nip,CONCAT_WS(' ',nama_depan,nama_belakang) as nama_lengkap, j.nama_jabatan, k.pendidikan_terakhir, 2000000 as tunjangan_pendidikan,
gaji_pokok+tunjangan_jabatan+2000000 as total_gaji
FROM tb_karyawan as k
JOIN tb_pekerjaan as p on k.nip = p.nip
JOIN tb_jabatan as j on p.kode_jabatan = j.kd_jabatan
JOIN tb_divisi as d on d.kd_divisi = p.kode_divisi
WHERE SUBSTRING(pendidikan_terakhir,1,2) = ('S1')
ORDER BY nip asc
 
 --6. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus
SELECT k.nip,CONCAT_WS(' ',nama_depan,nama_belakang) as nama_lengkap, j.nama_jabatan, d.nama_divisi,
--CASE
--WHEN J.kd_jabatan = 'MGR' THEN 0.25*((gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*7)
--WHEN J.kd_jabatan = 'ST' THEN 0.25*((gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*5)
--ELSE 0.25*((gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*2)
--END
CASE J.kd_jabatan
WHEN 'MGR' THEN 0.25*((gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*7)
WHEN 'ST' THEN 0.25*((gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*5)
ELSE 0.25*((gaji_pokok+tunjangan_jabatan+tunjangan_kinerja)*2)
END
as bonus
FROM tb_karyawan as k
JOIN tb_pekerjaan as p on k.nip = p.nip
JOIN tb_jabatan as j on p.kode_jabatan = j.kd_jabatan
JOIN tb_divisi as d on d.kd_divisi = p.kode_divisi
order by nip asc

--7. Buatlah kolom nip pada table karyawan sebagai kolom unique
ALTER TABLE tb_karyawan add CONSTRAINT nip_unique unique(nip)
ALTER TABLE tb_karyawan DROP CONSTRAINT nip_unique unique(nip)

--8. Buatlah kolom nip pada table karyawan sebagai index
CREATE INDEX index_nip2 on tb_karyawan(nip)

--9. Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan kondisi nama belakang di awali dengan huruf W
SELECT CONCAT_WS(' ',nama_depan,nama_belakang) as nama_lengkap, UPPER(nama_belakang) as namabelakang_kapital
FROM tb_karyawan
--WHERE nama_belakang like 'W%'
WHERE SUBSTRING(nama_belakang,1,1) in ('W')


--10.  Perusahaan akan memberikan bonus sebanyak 10% dari total gaji bagi karyawan yg sudah join di peruashaan diatas sama dengan 8 tahun
--Tampilkan nip, nama lengkap, jabatan, nama divisi, total gaji , bonus, lama bekerja
SELECT CONCAT_WS(' ',nama_depan,nama_belakang) as nama_lengkap, k.tgl_masuk, j.nama_jabatan, d.nama_divisi,
gaji_pokok+tunjangan_jabatan+tunjangan_kinerja as total_gaji,

(gaji_pokok+tunjangan_jabatan+tunjangan_kinerja) * (0.1) as bonus,
FLOOR(DATEDIFF(DAY,k.tgl_masuk, GETDATE())/365.25) as lama_bekerja
FROM tb_karyawan as k
JOIN tb_pekerjaan as p on k.nip = p.nip
JOIN tb_jabatan as j on p.kode_jabatan = j.kd_jabatan
JOIN tb_divisi as d on d.kd_divisi = p.kode_divisi
WHERE FLOOR(DATEDIFF(DAY,k.tgl_masuk, GETDATE())/365.25) >= 8


select * from tb_karyawan
select * from tb_divisi
select * from tb_jabatan
select * from tb_pekerjaan
