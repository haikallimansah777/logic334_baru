---TugasSQLDay01---

---Membuat Database---
CREATE DATABASE DBPenerbit

---1. Buat tabel Pengarang tblPengarang---
CREATE TABLE tblPengarang(
	ID int PRIMARY KEY identity (1,1) NOT NULL,
	Kd_Pengarang varchar(7) NOT NULL,
	Nama varchar(30) NOT NULL,
	Alamat varchar(80) NOT NULL,
	Kota varchar(15) NOT NULL,
	Kelamin Varchar(1) NOT NULL
)

---2. Masukan data kedalam tabel tblPengarang
INSERT INTO tblPengarang(Kd_Pengarang,Nama,Alamat,Kota,Kelamin)
VALUES
('P0001','Ashadi','Jl. Beo 25','Yogya','P'),
('P0002','Rian','Jl. Solo 123','Yogya','P'),
('P0003','Suwadi','Jl. Semangka 13','Bandung','P'),
('P0004','Siti','Jl. Durian 15','Solo','W'),
('P0005','Amir','Jl. Gajah 33','Kudus','P'),
('P0006','Suparman','Jl. Harimau 25','Jakarta','P'),
('P0007','Jaja','Jl. Singa 7','Bandung','P'),
('P0008','Saman','Jl. Naga 12','Yogya','P'),
('P0009','Anwar','Jl. Tidar 6A','Magelang','P'),
('P0010','Fatmawati','Jl. Renjana 4','Bogor','W')

---1. Buat tabel Gaji tblGaji
CREATE TABLE tblGaji(
	ID int PRIMARY KEY identity (1,1) NOT NULL,
	Kd_Pengarang varchar(7) NOT NULL,
	Nama varchar(30) NOT NULL,
	Gaji decimal(18,4) NOT NULL
)

---2. Masukan data kedalam tblGaji
INSERT INTO tblGaji(Kd_Pengarang,Nama,Gaji)
VALUES
('P0002','Rian','600000'),
('P0005','Amir','700000'),
('P0004','Siti','500000'),
('P0003','Suwadi','1000000'),
('P0010','Fatmawati','600000'),
('P0008','Saman','750000')

---1. Hitung dan tampilkan jumlah pengarang dari table tblPengarang
SELECT COUNT(id)
FROM tblPengarang

---2. Hitung berapa jumlah pengarang Wanita dan Pria
SELECT COUNT(id) as JumlahPengarang, Kelamin
FROM tblPengarang
GROUP BY Kelamin
ORDER BY Kelamin DESC

---3. Tampilkan record kota dan jumlah kotanya dari tabel tblPengarang
SELECT Kota, COUNT(Kota) as jumlah
FROM tblPengarang
GROUP BY Kota

---4. Tampilkan record kota diatas 1 kota dari tabel tblPengarang
SELECT Kota, COUNT(Kota) as jumlah
FROM tblPengarang
GROUP BY Kota
HAVING COUNT(Kota) > 1

---5. Tampilkan Kd_Pengarang yang terbesar dan terkecil dari tabel tblPengarang
SELECT MAX(Kd_Pengarang) AS Terbesar, MIN(Kd_Pengarang) AS Terkecil
FROM tblPengarang

---6. Tampilkan gaji tertinggi dan terendah
SELECT MAX(Gaji) as Gaji_Terbesar, MIN(Gaji) as Gaji_Terkecil
FROM tblGaji

---7. Tampilkan gaji diatas 600.000
SELECT Kd_Pengarang, Gaji as Gaji_diAtas_600Ribu
FROM tblGaji
WHERE Gaji > 600000

---8. Tampilkan jumlah gaji
SELECT SUM(gaji)
FROM tblGaji

---9. Tampilkan jumlah gaji berdasarkan kota
SELECT SUM(G.Gaji) AS Jumlah_Gaji, P.Kota
FROM tblPengarang AS P
JOIN tblGaji AS G on P.kd_pengarang = G.kd_pengarang
GROUP BY P.Kota


--10. Tampilkan seluruh record pengarang antara P0003-P0006 dari tabel pengarang.
SELECT *
FROM tblPengarang
--WHERE Kd_Pengarang >= P0003 and <= P0006
WHERE ID between 0003 and 0006

--11. Tampilkan seluruh data yogya, solo, dan magelang dari tabel pengarang.
SELECT *
FROM tblPengarang
--WHERE Kota = 'Yogya' OR Kota = 'Solo' OR Kota = 'Magelang'
WHERE Kota IN ('Yogya', 'Solo','Magelang')
ORDER BY Kota DESC

--12. Tampilkan seluruh data yang bukan yogya dari tabel pengarang.
SELECT *
FROM tblPengarang
WHERE Kota != 'Yogya'

--13. Tampilkan seluruh data pengarang yang nama (terpisah):
--a. dimulai dengan huruf [A]
SELECT *
FROM tblPengarang
WHERE Nama LIKE 'A%'
--b. berakhiran [i]
SELECT *
FROM tblPengarang
WHERE Nama LIKE '%i'
--c. huruf ketiganya [a]
SELECT *	
FROM tblPengarang
WHERE Nama LIKE '__a%'
--d. tidak berakhiran [n]
SELECT *
FROM tblPengarang
WHERE Nama not LIKE '%n'

--14. Tampilkan seluruh data table tblPengarang dan tblGaji dengan Kd_Pengarang yang sama
SELECT *
FROM tblPengarang AS P
JOIN tblGaji AS G on P.kd_pengarang = G.kd_pengarang
WHERE P.Kd_Pengarang = G.Kd_Pengarang

--15. Tampilan kota yang memiliki gaji dibawah 1.000.000
SELECT G.Gaji AS Jumlah_Gaji, P.Kota
FROM tblPengarang AS P
JOIN tblGaji AS G on P.kd_pengarang = G.kd_pengarang
WHERE Gaji < 1000000

--16. Ubah panjang dari tipe kelamin menjadi 10
ALTER TABLE tblPengarang ALTER COLUMN Kelamin varchar(10) NOT NULL

--17. Tambahkan kolom [Gelar] dengan tipe Varchar (12) pada tabel tblPengarang
ALTER TABLE tblPengarang ADD Gelar varchar(12)

--18. Ubah alamat dan kota dari Rian di table tblPengarang menjadi, Jl. Cendrawasih 65 dan Pekanbaru
UPDATE tblPengarang 
SET Alamat = 'Jl. Cendrawasih 65', Kota = 'Pekanbaru'
WHERE Nama = 'Rian'


--19. Buatlah view untuk attribute Kd_Pengarang, Nama, Kota, Gaji dengan nama vwPengarang
CREATE VIEW vwPengarang AS
SELECT P.Kd_Pengarang, P.Nama, P.Kota, G.Gaji 
FROM tblPengarang AS P
JOIN tblGaji AS G on P.kd_pengarang = G.kd_pengarang

SELECT *
FROM vwPengarang
