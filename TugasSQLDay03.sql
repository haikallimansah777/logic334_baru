---Create Database---
CREATE DATABASE DB_Sales

--Create Table---
CREATE TABLE SALESPERSON(
	ID int PRIMARY KEY identity (1,1) NOT NULL,
	NAME varchar(100) NOT NULL,
	BOD date NOT NULL,
	SALARY decimal(18,2) NOT NULL
)

CREATE TABLE ORDERS(
	ID int PRIMARY KEY identity (1,1) NOT NULL,
	ORDER_DATE DATE NOT NULL,
	CUST_ID INT NOT NULL,
	SALESPERSON_ID INT NOT NULL,
	AMOUNT decimal (18,2) NOT NULL
)

---Insert Data
INSERT INTO SALESPERSON(NAME, BOD, SALARY)
VALUES
('Abe','9/11/1988','140000'),
('Bob','9/11/1978','44000'),
('Chris','9/11/1983','40000'),
('Dan','9/11/1980','52000'),
('Ken','9/11/1977','115000'),
('Joe','9/11/1990','38000')

INSERT INTO ORDERS(ORDER_DATE, CUST_ID, SALESPERSON_ID, AMOUNT)
VALUES
('8/2/2020','4','2','540'),
('1/22/2021','4','5','1800'),
('7/14/2019','9','1','460'),
('1/29/2018','7','2','2400'),
('2/3/2021','6','4','600'),
('3/2/2020','6','4','720'),
('5/6/2021','9','4','150')

--a. Informasi nama sales yang memiliki order lebih dari 1.
SELECT S.NAME, COUNT(O.SALESPERSON_ID) as OrderLebihDari1
FROM SALESPERSON as S
JOIN ORDERS AS O on O.SALESPERSON_ID = S.ID
GROUP BY S.NAME
HAVING COUNT(O.SALESPERSON_ID) > 1

--b. Informasi nama sales yang total amount ordernya di atas 1000.
SELECT S.NAME, SUM(O.AMOUNT) as TotalAmountdiAtas1000
FROM SALESPERSON as S
JOIN ORDERS AS O on O.SALESPERSON_ID = S.ID
GROUP BY S.NAME
HAVING SUM(O.AMOUNT) > 1000

--c. Informasi nama sales, umur, gaji dan total amount order yang tahun ordernya >= 2020 dan data ditampilan berurut sesuai dengan umur (ascending).
--SELECT S.NAME, DATEDIFF(year,S.BOD, GETDATE()) as UMUR, S.SALARY, SUM(O.AMOUNT) as TotalAmountOrderdiAtas2020
SELECT S.NAME, FLOOR(DATEDIFF(DAY,S.BOD, GETDATE())/365.25) as UMUR, S.SALARY, SUM(O.AMOUNT) as TotalAmountOrderdiAtas2020
FROM SALESPERSON as S
JOIN ORDERS AS O on O.SALESPERSON_ID = S.ID
WHERE YEAR(O.ORDER_DATE) >= 2020
GROUP BY S.NAME, S.SALARY, FLOOR(DATEDIFF(DAY,S.BOD, GETDATE())/365.25)
ORDER BY UMUR ASC

--d. Carilah rata-rata total amount masing-masing sales urutkan dari hasil yg paling besar
SELECT S.NAME, AVG(O.AMOUNT) as AverageAmount
FROM SALESPERSON as S
JOIN ORDERS AS O on O.SALESPERSON_ID = S.ID
GROUP BY S.NAME
ORDER BY AverageAmount DESC

--e. Perusahaan akan memberikan bonus bagi sales yang berhasil memiliki order lebih dari 2 dan total order lebih dari 1000 sebanyak 30% dari salary
SELECT S.NAME, COUNT(O.SALESPERSON_ID) as JumlahOrder, SUM(O.AMOUNT) as TotalAmountdiAtas1000, 0.3*S.SALARY as Bonus
FROM SALESPERSON as S
JOIN ORDERS AS O on O.SALESPERSON_ID = S.ID
GROUP BY S.NAME, s.SALARY
HAVING COUNT(O.SALESPERSON_ID) > 2 AND SUM(O.AMOUNT) > 1000

SELECT NAME, COUNT(O.SALESPERSON_ID) as JumlahOrder, SUM(O.AMOUNT) as TotalAmountdiAtas1000,
CASE
	WHEN COUNT(O.SALESPERSON_ID) > 2 AND SUM(O.AMOUNT) > 1000 THEN 0.3*S.SALARY
	ELSE 0
END
AS Bonus
FROM SALESPERSON AS S
JOIN ORDERS AS O on S.ID = O.SALESPERSON_ID
GROUP BY S.NAME, S.SALARY
HAVING COUNT(O.SALESPERSON_ID) > 2 AND SUM(O.AMOUNT) > 1000
--ORDER BY Bonus DESC

--f. Tampilkan data sales yang belum memiliki orderan sama sekali
SELECT S.NAME
FROM SALESPERSON as S
LEFT JOIN ORDERS AS O on O.SALESPERSON_ID = S.ID
WHERE O.SALESPERSON_ID is null

--g. Gaji sales akan dipotong jika tidak memiliki orderan,  gaji akan di potong sebanyak 2%
SELECT S.NAME, s.SALARY as GajiAwal, 0.02*s.SALARY as PotonganGaji, s.SALARY - (0.02*s.SALARY) as GajiAkhir
FROM SALESPERSON as S
LEFT JOIN ORDERS AS O on O.SALESPERSON_ID = S.ID
WHERE O.SALESPERSON_ID is null


SELECT * FROM ORDERS
SELECT * FROM SALESPERSON