------------------------------------------
---|||DDL (Data Definition Language)|||---
------------------------------------------

---[create database]---
CREATE DATABASE db_kampus

---pindah database---
USE db_kampus

---[create table]----
CREATE TABLE mahasiswa(
	id bigint PRIMARY KEY identity (1,1),
	name varchar(50) NOT NULL,
	address varchar(50) NOT NULL,
	email varchar(255) NULL
)

---[create view]---
CREATE VIEW vw_mahasiswa
as SELECT * FROM mahasiswa

---[select view]---
select * from vw_mahasiswa 

---[create alter]---
---add column---
ALTER TABLE mahasiswa ADD nomor_hp varchar(100) NOT NULL

---drop column---
ALTER TABLE mahasiswa DROP COLUMN nomor_hp

---alter column---
ALTER TABLE mahasiswa ALTER COLUMN email varchar(100) NOT NULL

---[drop database]---
DROP DATABASE [nama database]

---[drop table]---
DROP TABLE [nama_tabel]

---[drop view]---
DROP VIEW [nama_view]

-------------------------------------------
---|||DML (Data Manipulation Language|||---
-------------------------------------------	

---insert---
INSERT INTO mahasiswa(name,address,email)
VALUES
('Haikal','Kuningan','haikal5nsah@gmail.com'),
('Imam','Bekasi','imamassidqi@gmail.com'),
('Irvan','Jakarta','aliirvan122@gmail.com'),
('Rezky','Cengkareng','rezkyfazri08@gmail.com'),
('Tunggul','Semarang','tunggulyudhaputra5@gmail.com'),
('Shabrina','Palembang','shabrinaputrif1604@gmail.com')

---select---
SELECT id,name,address,email FROM mahasiswa

---update---
UPDATE mahasiswa set name = 'Haikal Limansah' where id = 1

---delete---
DELETE mahasiswa WHERE name = 'tes'

---------------------------||JOIN||--------------------------
CREATE TABLE biodata(
	id bigint PRIMARY KEY identity (1,1),
	mahasiswa_id bigint NULL,
	tgl_lahir date NULL,
	gender varchar(10) NULL
)

INSERT INTO biodata (mahasiswa_id, tgl_lahir, gender)
VALUES
(1, '2020-06-10','Pria'),
(2, '2021-07-10','Pria'),
(3, '2022-08-10','Wanita')



-	--[JOIN (AND OR NOT)]---
SELECT mhs.id AS id_mahasiswa, mhs.name AS nama_mahasiswa, mhs.email AS email, bio.tgl_lahir AS tanggal_lahir, bio.gender AS gender
FROM mahasiswa AS mhs
JOIN biodata AS bio ON mhs.id = bio.mahasiswa_id
WHERE mhs.id = 2 AND mhs.name = 'Imam' OR NOT  mhs.name = 'Irvan'

---[ORDER BY]----
SELECT *
FROM mahasiswa
ORDER BY name DESC

---[TOP]---
SELECT TOP 3 *
FROM mahasiswa
ORDER BY id ASC

---[BETWEEN]---
SELECT *
FROM mahasiswa
WHERE id between 3 and 5
--WHERE id >= 3 and id <= 5

---[LIKE]---
SELECT *
FROM mahasiswa
WHERE
name LIKE 'i%' --awalan i
--name LIKE '%a' --akhiran a
--name LIKE '%ez%' --mengandung ez(containts)
--name LIKE '_a%' --karakter ke dua a, minimal 2 karakter
--name LIKE 'a__%' --awalan a minimal 3 karakter
--name LIKE 'i%n' --awalan i dan akhiran n

---[GROUP BY]---
---Berfungsi untuk menghilangkan duplikat---
SELECT name
FROM mahasiswa
GROUP BY name --incase data tunggul diinput 2x

---[AGGREGATE FUNCTION]---
---(SUM, COUNT, AVG, MIN , MAX)---
SELECT SUM(id)
FROM mahasiswa

SELECT SUM(id), name, address
FROM mahasiswa
GROUP BY name,address --incase data tunggul diinput 2x. Ketika diselect sum(id) yang ditampilkan hanya milik tunggul karena memiliki 2 data yang sama sedangkan yang lain tidak bisa disum.

---[HAVING]---
SELECT sum(id), name, address
FROM mahasiswa
GROUP BY name, address
HAVING
