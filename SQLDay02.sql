---SQL DAY 2 ---
select * from mahasiswa
--IN
SELECT *
FROM mahasiswa
WHERE name in ('Tunggul','Irvan')

--Add column nilai pada tabel mahasiswa
ALTER TABLE mahasiswa add nilai int

--Update column nilai
UPDATE mahasiswa set nilai = 80 where id = 1
UPDATE mahasiswa set nilai = 80 where id = 2
UPDATE mahasiswa set nilai = 70 where id = 3
UPDATE mahasiswa set nilai = 100 where id = 4
UPDATE mahasiswa set nilai = 50 where id = 5
UPDATE mahasiswa set nilai = 40 where id = 6

--AVG
SELECT AVG(nilai)
FROM mahasiswa

--INNER JOIN ATAU JOIN
select * from mahasiswa
select * from biodata

SELECT *
FROM mahasiswa AS mhs
INNER JOIN biodata AS bio ON mhs.id = bio.mahasiswa_id

--LEFT JOIN
SELECT *
FROM mahasiswa AS mhs
LEFT JOIN biodata AS bio ON mhs.id = bio.mahasiswa_id
WHERE bio.id is NULL

--RIGHT JOIN
SELECT *
FROM mahasiswa AS mhs
RIGHT JOIN biodata AS bio ON mhs.id = bio.mahasiswa_id
WHERE mhs.id is NULL

--DISTINCT (untuk menghilangkan duplikat)
SELECT DISTINCT *
FROM mahasiswa

--GROUP BY
SELECT name
FROM mahasiswa
GROUP BY name

--SUBSTRING
SELECT SUBSTRING('SQL Tutorial',1,3)

--CHARINDEX
SELECT CHARINDEX('t','Customer')

--DATALENGTH
SELECT DATALENGTH('W3schools.com')

--CASE WHEN
SELECT name, nilai,
CASE
WHEN nilai >= 80 THEN 'A'
WHEN nilai >= 60 THEN 'B'
ELSE 'C'
END
as grade
FROM mahasiswa

--CONCAT
SELECT CONCAT ('HAIKAL ','LIMANSAH ','WOW ','KEREN ')
SELECT 'HAIKAL' + 'LIMANSAH' + 'WOW' + 'KEREN'
SELECT CONCAT ('Nama : ',name) from mahasiswa

--OPERATOR ARITMATIKA
CREATE TABLE penjualan(
id int PRIMARY KEY identity (1,1),
name varchar(50) NOT NULL,
harga decimal(18,0) NOT NULL
)

INSERT INTO penjualan(name, harga)
VALUES
('Indomie',1500),
('Close-up',3500),
('Pepsodent',3000),
('Brush Formula',2500),
('Roti Manis',1000),
('Gula',3500),
('Sarden',4500),
('Rokok Sampoerna',11000),
('Rokok 234',11000)

SELECT name, harga, harga*100 as [harga * 100]
FROM penjualan

--GETDATE
SELECT GETDATE() AS WaktuHariIni

--DAY MONTH YEAR
SELECT DAY('2023-11-10') AS tanggal
SELECT MONTH('2023-11-10') AS bulan
SELECT YEAR('2023-11-10') AS tahun
SELECT MONTH (GETDATE()) AS BulanIni

--DATE ADD
SELECT DATEADD(year,1,'2023-11-10')
SELECT DATEADD(month,3,'2023-11-10')
SELECT DATEADD(day,4,'2023-11-10')
SELECT DATEADD(HOUR,3,'2023-11-10')
SELECT DATEADD(month,3,GETDATE())

--DATE DIFF
SELECT DATEDIFF(year,'1999-03-12', GETDATE())
SELECT DATEDIFF(MONTH,'1999-03-12', GETDATE())
SELECT DATEDIFF(DAY,'1999-03-12', DATEADD(DAY,5,GETDATE() ))

--SUB QUERY
SELECT *
FROM mahasiswa mhs
left join biodata bio on mhs.id = bio.mahasiswa_id
where mhs.nilai = (select max (nilai) from mahasiswa)

INSERT INTO mahasiswa_new(name,address,email)
SELECT name,address,email from mahasiswa

--CREATE VIEW
CREATE VIEW vw_mahasiswa
as
SELECT name
FROM mahasisiwa
WHERE name = 'banu'

--ALTER VIEW
ALTER VIEW vw_mahasiswa
as
SELECT name, address, nilai
FROM mahasiswa
WHERE name != 'banu'

--DROP VIEW
DROP VIEW vw_mahasiswa

--SELECT VIEW
SELECT *
FROM vw_mahasiswa

--CREATE INDEX
CREATE INDEX index_nameemail on mahasiswa(name,email)

--CREATE UNIQUE INDEX
CREATE UNIQUE INDEX index_id on mahasiswa(id)

--DROP INDEX
DROP index index_id on mahasiswa

--PRIMARY KEY
CREATE TABLE coba(id int not null, nama varchar(50) not null)
ALTER TABLE coba ADD CONSTRAINT pk_idnama PRIMARY KEY(id,nama)

--DROP PK
ALTER TABLE COBA
DROP CONSTRAINT pk_idnama

--UNIQUE KEY
ALTER TABLE coba
ADD CONSTRAINT unique_nama unique(nama)

--DROP UNIQUE KEY
ALTER TABLE coba
DROP CONSTRAINT unique_nama

--FOREIGN KEY
ALTER TABLE biodata
ADD CONSTRAINT fk_mahasiswa_id
FOREIGN KEY(mahasiswa_id)
REFERENCES mahasiswa(id)

--DROP FOREIGN KEY
ALTER TABLE biodata
DROP CONSTRAINT fk_mahasiswa_id